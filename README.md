# uprompt

dmenu-style menu utility for wayland.
Currently works on river and dwl, but not Niri.

Read manpage for keybindings and usage flags.

## LIst of Modes

- Input line
- Horizontal list selection with filter
- Vertical list selection with filter
- Grid Item selection with filter

### Selection Filter

The filter is inspired by dmenu and by xmonad's built-in prompt which
automatically generates an item grid.

## Implementation Details

Originally, this started as a fork of [dmenu-wl](https://github.com/nyyManni/dmenu-wayland).
The program is wayland only and software-rendered.

## Libraries

For drawing, pixman is used.
For font rendering and management, freetype, fontconfig with fcft.

## Dependencies

Requires pixman, freetype, fontconfig and fcft installed as shared libraries.
To generate man-pages, it uses scdoc.
The build system is ninja (or samurai).

## Building

Run `samu` or `ninja` in the repo directory.
Configure built-in settings in config.h

## Future plans

Integrate waybutt and waypass into uprompt.
