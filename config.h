static const uint8_t MaxClipboardLen = 160; // Max byte length for clipboard contents
static const char *ClipboardCommand = "wl-paste"; // alt: wl-paste

/* Currently does not do anything */
#define UNDERSCORE_CURSOR 1 // 0 for vertical line cursor

#define FONT_NUMBER 4
static const char *FnNames[FONT_NUMBER] = {
	"monospace:pixelsize=13:style=Bold:antialias=true",
	"Koruri:pixelsize=13:Semibold:antialias=true",
	"Blobmoji:pixelsize=13:antialias=true:autohint=true",
	"Symbola:pixelsize=13:antialias=true:autohint=true",
};

static const uint32_t ColBg       = 0x0C0C0CFF;
static const uint32_t ColFg       = 0x8787A6ff;
static const uint32_t ColSelBg    = 0x0C0C0CFF;
static const uint32_t ColSelFg    = 0xA39CD9ff;
static const uint32_t ColPromptBg = 0x0C0C0CFF;
static const uint32_t ColPromptFg = 0xEB8BB2ff;
static const uint32_t ColCursor   = 0xEBA28Bff;
static const uint32_t ColBorder   = 0x161616ff;

static const uint16_t DimLineHeight = 22;
static const uint16_t DimTextLevel  = 16; // text level from top of line
static const uint16_t DimInputWidth = 270;
static const uint8_t  BorderWidth   = 5;
