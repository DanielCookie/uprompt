struct pool_buffer {
	struct wl_buffer *buffer;
	pixman_image_t *pixman;
	size_t size;
	int32_t width, height, scale;
	void *data;
	bool busy;
};

struct pool_buffer *get_next_buffer(struct wl_shm *shm,
struct pool_buffer pool[static 2], int32_t width, int32_t height,
int32_t scale);
