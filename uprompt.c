#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <poll.h>
#include <sys/mman.h>
#include <unistd.h>
#include <xkbcommon/xkbcommon.h>
#include <assert.h>
#include <sys/timerfd.h>
#include <fontconfig.h>
#include <pixman.h>
#include <fcntl.h>
#include <fcft/fcft.h>

#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "xdg-output-unstable-v1-client-protocol.h"

#include "utf8.h"
#include "pool_buffer.h"
#include "config.h"

enum {
	RunFailure = -1,
	RunSuccess = 0,
	RunRunning = 1,
};

enum {
	ExitSuccess = 0,
	ExitFailure = 1,
};

enum {
	ModeNone    = 0,  ModeRestrictRet = 1,
	ModeInstant = 2,  ModeBottom      = 8,
	ModeEnter   = 16, ModeHoriz       = 32,
	ModeVert    = 64, ModeGrid        = 128,
};

struct uprompt_item {
	char *text;
	uint16_t len;
	uint16_t width;
	struct uprompt_item *next;         // traverses all items
	struct uprompt_item *left, *right; // traverses matching items
};

struct output {
	struct uprompt_state *prompt;
	struct wl_output *output;
	struct zxdg_output_v1 *xdg_output;
	int32_t scale;
};

struct pix_canvas {
	pixman_color_t bg;
	pixman_color_t sel_bg;
	pixman_color_t prompt_bg;
	pixman_color_t cursor;
	pixman_color_t border;
	pixman_image_t *fg;
	pixman_image_t *sel_fg;
	pixman_image_t *prompt_fg;
};

struct hex_colors {
	uint32_t bg;        uint32_t fg;
	uint32_t prompt_bg; uint32_t prompt_fg;
	uint32_t sel_bg;    uint32_t sel_fg;
	uint32_t cursor;    uint32_t border;
};

struct dimensions {
	uint8_t  line_height;   uint8_t  text_level;
	uint8_t  padding;       uint8_t  border_width;
	uint8_t  arrow_width;   uint8_t  lines;
	uint16_t width;         uint16_t height;
	uint16_t prompt_width;  uint16_t input_width;
	uint16_t max_item_width;
};

struct uprompt_state {
	struct output *output;
	char *output_name;

	struct wl_display *display;
	struct wl_compositor *compositor;
	struct wl_shm *shm;
	struct wl_seat *seat;
	struct wl_keyboard *keyboard;
	struct zwlr_layer_shell_v1 *layer_shell;
	struct zxdg_output_manager_v1 *output_manager;

	struct wl_surface *surface;
	struct zwlr_layer_surface_v1 *layer_surface;

	struct xkb_context *xkb_context;
	struct xkb_keymap *xkb_keymap;
	struct xkb_state *xkb_state;

	struct pool_buffer buffers[2];
	struct pool_buffer *current;

	uint8_t mode;

	struct hex_colors hex;
	struct dimensions dim;
	struct pix_canvas canvas;

	int (*fstrncmp) (const char *, const char *, size_t);
	const char **fn_names[FONT_NUMBER];
	char *prompt_str;
	struct fcft_text_run *prompt_run;
	struct fcft_font *font;

	char text[BUFSIZ];
	size_t cursor;

	int32_t repeat_timer;
	int32_t repeat_delay;
	int32_t repeat_period;
	uint32_t repeat_key;
	enum wl_keyboard_key_state repeat_key_state;
	xkb_keysym_t repeat_sym;

	int8_t run;

	struct uprompt_item *items;
	struct uprompt_item *matches;
	struct uprompt_item *selection;
	struct uprompt_item *leftmost, *rightmost;

	void (*scroll_matches) (struct uprompt_state *us);
	void (*draw_items) (pixman_image_t *, struct uprompt_state *,
		struct dimensions *, struct pix_canvas *, struct fcft_font *, bool);
};

// Principal functions
static void uprompt_init(struct uprompt_state *us, struct dimensions *dim);
static void read_stdin(struct uprompt_state *us);
static void uprompt_create_surface(struct uprompt_state *us);
static struct pix_canvas canvas_init(struct hex_colors *hex);
static void font_init(struct uprompt_state *us);
static void uprompt_fini(struct uprompt_state *us);
static void free_items(struct uprompt_state *us);
static inline void surface_fini(struct uprompt_state *us);
static inline void canvas_fini(struct pix_canvas *canvas);
static inline void font_fini(struct uprompt_state *us);
static inline void compositor_fini(struct uprompt_state *us);

static void insert(struct uprompt_state *us, const char *s, ssize_t n);
static char *fstrstr(struct uprompt_state *us, const char *s, const char *sub);
static void append_item(struct uprompt_item *item,
	struct uprompt_item **list, struct uprompt_item **last);
static inline void type_rune(struct uprompt_state *us, xkb_keysym_t sym, char *buf);
static void match(struct uprompt_state *us);
static void insert(struct uprompt_state *us, const char *s, ssize_t n);
static void draw_frame(struct uprompt_state *us);
static size_t next_rune(struct uprompt_state *us, int8_t incr);
static size_t next_word(struct uprompt_state *us, int8_t incr);
static void keypress(struct uprompt_state *us,
	enum wl_keyboard_key_state key_state, xkb_keysym_t sym);

static void scroll_grid_matches(struct uprompt_state *us);
static void scroll_vertical_matches(struct uprompt_state *us);
static void scroll_horizontal_matches(struct uprompt_state *us);

static void draw_horizontal_items(pixman_image_t *pix_buf, struct uprompt_state *us,
	struct dimensions *d, struct pix_canvas *c, struct fcft_font *font, bool bottom);
static void draw_vertical_items(pixman_image_t *pix_buf, struct uprompt_state *us,
	struct dimensions *d, struct pix_canvas *c, struct fcft_font *font, bool bottom);
static void draw_grid_items(pixman_image_t *pix_buf, struct uprompt_state *us,
	struct dimensions *d, struct pix_canvas *c, struct fcft_font *font, bool bottom);

static void noop() {}

static void eprintf(const char *fmt, ...) {
	va_list ap;

	fprintf(stderr, "%s: ", PROGNAME);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(1);
}

static inline void fill_box_solid(pixman_image_t *pixman, pixman_color_t *color,
uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2) {

	pixman_image_fill_boxes(PIXMAN_OP_SRC, pixman, color, 1,
		&(pixman_box32_t) {.x1 = x1, .x2 = x2, .y1 = y1, .y2 = y2});
}

static inline void fill_box_transparent(pixman_image_t *pixman,
uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2) {

	fill_box_solid(pixman, &(pixman_color_t) {.red = 0,
		.green = 0, .blue = 0, .alpha = 0}, x1, x2, y1, y2);
}

static inline void draw_glyph(pixman_image_t *pixman,
const struct fcft_glyph *rune, pixman_image_t *fill, uint16_t x, uint16_t y) {

	if (pixman_image_get_format(rune->pix) == PIXMAN_a8r8g8b8) {
		pixman_image_composite32(PIXMAN_OP_OVER, rune->pix, NULL, pixman,
			0, 0, 0, 0, x + rune->x, y - rune->y, rune->width, rune->height);
	}
	else {
		pixman_image_composite32(PIXMAN_OP_OVER, fill, rune->pix, pixman,
			0, 0, 0, 0, x + rune->x, y - rune->y, rune->width, rune->height);
	}
}

static inline void draw_border_line(pixman_image_t *pixman,
struct uprompt_state *us, uint16_t y) {

	fill_box_solid(pixman, &us->canvas.border, 0,
		us->dim.width, y, y + us->dim.border_width);
}

static inline const pixman_color_t hex2pixman(uint32_t hex_color) {
	return (pixman_color_t) {
		.red   = (hex_color >> 0x18 & 0xFF) * 0xFF,
		.green = (hex_color >> 0x10 & 0xFF) * 0xFF,
		.blue  = (hex_color >> 0x8  & 0xFF) * 0xFF,
		.alpha = (hex_color         & 0xFF) * 0xFF,
	};
}

static inline bool has_mode_bottom(struct uprompt_state *us) {
	return (us->mode & ModeBottom) >> 3; // 3 -> Order of magnitude of ModeBottom in binary
}

static inline uint16_t top_vs_bottom(bool bottom,
uint16_t bottom_value, uint16_t top_value) {

	return bottom * bottom_value + !bottom * top_value;
}

static uint16_t get_text_advance(struct fcft_font *font, char *text) {
	uint16_t advance = 0;
	uint32_t codepoint, last_cp = 0, state = UTF8_ACCEPT;

	for (char *str_p = text; *str_p; str_p++) {
		if (utf8_decode(&state, &codepoint, *str_p)) continue;

		advance += (fcft_rasterize_char_utf32(font,
			codepoint, FCFT_SUBPIXEL_NONE))->advance.x;
	}

	return advance;
}

static void parse_hex_color(char *str, uint32_t *color) {
	if (str[0] == '#') str++;

	size_t len = strnlen(str, BUFSIZ);

	if (len != 6 && len != 8)
		eprintf("Color format must be '[#]rrggbb[aa]'\n");

	char *ptr;
	uint32_t _val = strtoul(str, &ptr, 16);
	if (*ptr != '\0') eprintf("Could not convert string to hex number");

	*color = len == 6 ? ((_val << 8) | 0xff) : _val;
}

static void set_line_height(struct dimensions *dim, char *height_str) {
	uint8_t i;
	for (i = 0; i <= 3; i++) if (height_str[i] == ':') break;

	if (2 < i) dim->line_height = atoi(height_str);
	else {
		height_str[i] = '\0';
		dim->line_height = atoi(height_str);
		dim->text_level = dim->line_height - atoi(height_str + ++i);
	}
}

static void surface_enter(void *data, struct wl_surface *surface,
struct wl_output *wl_output) {

	struct uprompt_state *us = data;
	us->output = wl_output_get_user_data(wl_output);
	wl_surface_set_buffer_scale(us->surface, us->output->scale);
	wl_surface_commit(us->surface);
	draw_frame(us);
}

static const struct wl_surface_listener surface_listener = {
	.enter = surface_enter,
	.leave = noop,
};

static void layer_surface_configure(void *data, struct zwlr_layer_surface_v1 *surface,
uint32_t serial, uint32_t width, uint32_t height) {

	struct uprompt_state *us = data;
	us->dim.width = width;
	us->dim.height = height;
	zwlr_layer_surface_v1_ack_configure(surface, serial);
}

static void layer_surface_closed(void *data, struct zwlr_layer_surface_v1 *surface) {

	struct uprompt_state *us = data;
	us->run = RunFailure;
}

static struct zwlr_layer_surface_v1_listener layer_surface_listener = {
	.configure = layer_surface_configure,
	.closed = layer_surface_closed,
};

static void output_scale(void *data, struct wl_output *wl_output, int32_t factor) {
	struct output *output = data;
	output->scale = factor;
}

static void output_name(void *data,
struct zxdg_output_v1 *xdg_output, const char *name) {

	struct output *output = data;
	struct uprompt_state *us = output->prompt;
	char *outname = us->output_name;

	if (!us->output && outname && strcmp(outname, name) == 0) us->output = output;
}

static struct wl_output_listener output_listener = {
	.geometry = noop,
	.mode = noop,
	.done = noop,
	.scale = output_scale,
};

static struct zxdg_output_v1_listener xdg_output_listener = {
	.logical_position = noop,
	.logical_size = noop,
	.done = noop,
	.name = output_name,
	.description = noop,
};

static void keyboard_keymap(void *data, struct wl_keyboard *wl_keyboard,
uint32_t format, int32_t fd, uint32_t size) {

	struct uprompt_state *us = data;

	if (format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1) {
		close(fd);
		us->run = RunFailure;
		return;
	}

	char *map_shm = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
	if (map_shm == MAP_FAILED) {
		close(fd);
		us->run = RunFailure;
		return;
	}

	us->xkb_keymap = xkb_keymap_new_from_string(us->xkb_context,
		map_shm, XKB_KEYMAP_FORMAT_TEXT_V1, 0);
	munmap(map_shm, size);
	close(fd);
	us->xkb_state = xkb_state_new(us->xkb_keymap);
}

static void keyboard_repeat(struct uprompt_state *us) {
	keypress(us, us->repeat_key_state, us->repeat_sym);
	struct itimerspec spec = { 0 };

	spec.it_value.tv_sec = us->repeat_period / 1000;
	spec.it_value.tv_nsec = (us->repeat_period % 1000) * 1000000l;
	timerfd_settime(us->repeat_timer, 0, &spec, NULL);
}

static void keyboard_key(void *data, struct wl_keyboard *wl_keyboard,
uint32_t serial, uint32_t time, uint32_t key, uint32_t _key_state) {

	struct uprompt_state *us = data;
	enum wl_keyboard_key_state key_state = _key_state;
	xkb_keysym_t sym = xkb_state_key_get_one_sym(us->xkb_state, key + 8);

	keypress(us, key_state, sym);

	if (key_state == WL_KEYBOARD_KEY_STATE_PRESSED && 0 <= us->repeat_period) {
		us->repeat_key_state = key_state;
		us->repeat_sym = sym;
		us->repeat_key = key;

		struct itimerspec spec = { 0 };
		spec.it_value.tv_sec = us->repeat_delay / 1000;
		spec.it_value.tv_nsec = (us->repeat_delay % 1000) * 1000000l;
		timerfd_settime(us->repeat_timer, 0, &spec, NULL);
	}
	else if (key_state == WL_KEYBOARD_KEY_STATE_RELEASED && key == us->repeat_key) {
		struct itimerspec spec = { 0 };
		timerfd_settime(us->repeat_timer, 0, &spec, NULL);
	}
}

static void keyboard_repeat_info(void *data,
struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay) {

	struct uprompt_state *us = data;
	us->repeat_delay = delay;

	if (0 < rate) us->repeat_period = 1000 / rate;
	else us->repeat_period = -1;
}

static void keyboard_modifiers(void *data, struct wl_keyboard *keyboard,
uint32_t serial, uint32_t mods_depressed, uint32_t mods_latched,
uint32_t mods_locked, uint32_t group) {

	struct uprompt_state *us = data;
	xkb_state_update_mask(us->xkb_state, mods_depressed, mods_latched,
		mods_locked, 0, 0, group);
}

static const struct wl_keyboard_listener keyboard_listener = {
	.keymap = keyboard_keymap,
	.enter = noop,
	.leave = noop,
	.key = keyboard_key,
	.modifiers = keyboard_modifiers,
	.repeat_info = keyboard_repeat_info,
};

static void seat_capabilities(void *data, struct wl_seat *seat,
enum wl_seat_capability caps) {

	struct uprompt_state *us = data;

	if (caps & WL_SEAT_CAPABILITY_KEYBOARD) {
		us->keyboard = wl_seat_get_keyboard(seat);
		wl_keyboard_add_listener(us->keyboard, &keyboard_listener, us);
	}
}

static const struct wl_seat_listener seat_listener = {
	.capabilities = seat_capabilities,
	.name = noop,
};

static void handle_global(void *data, struct wl_registry *registry,
uint32_t name, const char *interface, uint32_t version) {

	struct uprompt_state *us = data;

	if (strcmp(interface, wl_compositor_interface.name) == 0) {
		const uint32_t required = 4;
		assert(required <= version);
		us->compositor = wl_registry_bind(registry, name,
			&wl_compositor_interface, required);
	}
	else if (strcmp(interface, wl_shm_interface.name) == 0) {
		const uint32_t required = 1;
		assert(required <= version);
		us->shm = wl_registry_bind(registry, name, &wl_shm_interface, required);
	}
	else if (strcmp(interface, wl_seat_interface.name) == 0) {
		const uint32_t required = 4;
		assert(required <= version);
		struct wl_seat *seat = wl_registry_bind(registry, name,
			&wl_seat_interface, required);
		wl_seat_add_listener(seat, &seat_listener, us);
	}
	else if (strcmp(interface, zwlr_layer_shell_v1_interface.name) == 0) {
		const uint32_t required = 1;
		assert(required <= version);
		// printf("layer_shell, wanted version: %d, compositor implemented: %d", required, version);
		us->layer_shell = wl_registry_bind(registry, name,
			&zwlr_layer_shell_v1_interface, required);
	}
	else if (strcmp(interface, zxdg_output_manager_v1_interface.name) == 0) {
		const uint32_t required = 3;
		assert(required <= version);
		us->output_manager = wl_registry_bind(registry, name,
			&zxdg_output_manager_v1_interface, required);
	}
	else if (strcmp(interface, wl_output_interface.name) == 0) {
		const uint32_t required = 3;
		assert(required <= version);
		struct output *output = calloc(1, sizeof(struct output));

		output->output = wl_registry_bind(registry, name, &wl_output_interface, required);
		output->prompt = us;
		output->scale = 1;

		wl_output_set_user_data(output->output, output);
		wl_output_add_listener(output->output, &output_listener, output);

		if (us->output_manager != NULL) {
			output->xdg_output = zxdg_output_manager_v1_get_xdg_output(
				us->output_manager, output->output);
			zxdg_output_v1_add_listener(output->xdg_output,
				&xdg_output_listener, output);
		}
	}
}  static const struct wl_registry_listener registry_listener = {
	.global = handle_global,
	.global_remove = noop,
};

void draw_horizontal_items(pixman_image_t *pix_buf, struct uprompt_state *us,
struct dimensions *d, struct pix_canvas *c, struct fcft_font *font, bool bottom) {

	uint16_t y = bottom * d->border_width,
	         x = d->prompt_width + d->input_width;

	draw_border_line(pix_buf, us, !bottom * d->line_height);
	if (!us->matches) return;

	uint16_t left_arrow_pos = x, text_level = y + d->text_level;
	bool scroll_right = false;
	struct uprompt_item *item;
	const struct fcft_glyph *rune_p;
	char *str_p;
	uint32_t state = UTF8_ACCEPT, codepoint, prev;

	draw_border_line(pix_buf, us, !has_mode_bottom(us) * d->line_height);
	x += us->dim.arrow_width;

	for (item = us->leftmost; item; item = item->right) {
		if (d->width - d->arrow_width < x + d->padding + item->width) {
			scroll_right = true;
			break;
		}

		x += d->padding;

		if (us->selection == item) {
			fill_box_solid(pix_buf, &c->sel_bg, x - d->padding,
				x + item->width + d->padding, y, y + d->line_height);
		
			for (str_p = item->text; *str_p; str_p++) {
				if (utf8_decode(&state, &codepoint, *str_p)) continue;

				rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
				draw_glyph(pix_buf, rune_p, c->sel_fg, x, text_level);
				x += rune_p->advance.x;
			}
		}
		else {
			for (str_p = item->text; *str_p; str_p++) {
				if (utf8_decode(&state, &codepoint, *str_p)) continue;

				rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
				draw_glyph(pix_buf, rune_p, c->fg, x, text_level);
				x += rune_p->advance.x;
			}
		}

		x += d->padding;
	}

	if (us->leftmost != us->matches) {
		rune_p = fcft_rasterize_char_utf32(font, '<', FCFT_SUBPIXEL_NONE);
		draw_glyph(pix_buf, rune_p, c->fg,
			left_arrow_pos + 5 * d->padding / 2 - rune_p->x, text_level);
	}

	if (scroll_right) {
		rune_p = fcft_rasterize_char_utf32(font, '>', FCFT_SUBPIXEL_NONE);
		draw_glyph(pix_buf, rune_p, c->fg,
			d->width - 5 * d->padding / 2 - rune_p->width - rune_p->x, text_level);
	}
}

void draw_vertical_items(pixman_image_t *pix_buf, struct uprompt_state *us,
struct dimensions *d, struct pix_canvas *c, struct fcft_font *font, bool bottom) {

	uint16_t y = top_vs_bottom(bottom, d->border_width, d->line_height);

	if (!us->matches) {
		draw_border_line(pix_buf, us, top_vs_bottom(bottom,
			d->height - d->line_height - d->border_width, d->line_height));
		fill_box_transparent(pix_buf, 0, d->width,
			!bottom * (d->line_height + d->border_width),
				top_vs_bottom(bottom,
				d->height - d->line_height - d->border_width, d->height));
		return;
	}

	uint16_t x, input_start = d->prompt_width + d->padding,
	         height = d->height - d->border_width;
	struct uprompt_item *item;
	const struct fcft_glyph *rune_p;
	char *str_p;
	uint32_t state = UTF8_ACCEPT, codepoint;

	draw_border_line(pix_buf, us, !bottom * height);
	fill_box_solid(pix_buf, &c->bg, 0, d->width, y, y + height - d->line_height);

	for (item = us->leftmost; item; item = item->right) {
		x = input_start;

		if (us->selection == item) {
			fill_box_solid(pix_buf, &c->sel_bg, input_start - d->padding,
				input_start + item->width + d->padding, y, y + d->line_height);

			for (char *str_p = item->text; *str_p; str_p++) {
				if (utf8_decode(&state, &codepoint, *str_p)) continue;

				rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
				draw_glyph(pix_buf, rune_p, c->sel_fg, x, y + d->text_level);
				x += rune_p->advance.x;
			}
		}
		else {
			for (char *str_p = item->text; *str_p; str_p++) {
				if (utf8_decode(&state, &codepoint, *str_p)) continue;

				rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
				draw_glyph(pix_buf, rune_p, c->fg, x, y + d->text_level);
				x += rune_p->advance.x;
			}
		}

		y += d->line_height;
		if (top_vs_bottom(bottom, d->height - d->line_height, height) <= y) break;
	}
}

void draw_grid_items(pixman_image_t *pix_buf, struct uprompt_state *us,
struct dimensions *d, struct pix_canvas *c, struct fcft_font *font, bool bottom) {

	uint16_t y = top_vs_bottom(bottom, d->border_width, d->line_height);

	if (!us->matches) {
		draw_border_line(pix_buf, us, top_vs_bottom(bottom,
			d->height - d->line_height - d->border_width, d->line_height));
		fill_box_transparent(pix_buf, 0, d->width,
			!bottom * (d->line_height + d->border_width),
				top_vs_bottom(bottom,
				d->height - d->line_height - d->border_width, d->height));
		return;
	}

	uint16_t x, height = d->height - d->border_width;
	struct uprompt_item *item;
	const struct fcft_glyph *rune_p;
	char *str_p;
	uint32_t state = UTF8_ACCEPT, codepoint;
	size_t col;

	uint16_t column_width = d->max_item_width + 2 * d->padding;
	uint8_t columns = d->width / column_width;

	draw_border_line(pix_buf, us, !bottom * height);
	fill_box_solid(pix_buf, &c->bg, 0, d->width, y, y + height - d->line_height);

	for (item = us->leftmost; item;) {
		x = 0;

		for (col = 0; item && col < columns; col++, item = item->right) {
			x = column_width * col + d->padding;

			if (us->selection == item) {
				fill_box_solid(pix_buf, &c->sel_bg, x - d->padding,
					x + item->width + d->padding, y, y + d->line_height);

				for (str_p = item->text; *str_p; str_p++) {
					if (utf8_decode(&state, &codepoint, *str_p)) continue;

					rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
					draw_glyph(pix_buf, rune_p, c->sel_fg, x, y + d->text_level);
					x += rune_p->advance.x;
				}
			}
			else {
				for (str_p = item->text; *str_p; str_p++) {
					if (utf8_decode(&state, &codepoint, *str_p)) continue;

					rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
					draw_glyph(pix_buf, rune_p, c->fg, x, y + d->text_level);
					x += rune_p->advance.x;
				}
			}
		}

		y += d->line_height;
		if (top_vs_bottom(bottom, d->height - d->line_height, height) <= y) break;
	}
}

void draw_frame(struct uprompt_state *us) {
	us->current = get_next_buffer(us->shm, us->buffers,
		us->dim.width, us->dim.height, us->output ? us->output->scale : 1);

	if (!us->current) return;

	pixman_image_t *pix_buf = us->current->pixman;
	bool bottom = has_mode_bottom(us);

	struct fcft_font *font = us->font;
	struct pix_canvas *c = &us->canvas;
	struct dimensions *d = &us->dim;
	uint16_t x, y = bottom * (d->height - d->line_height), cursor_pos, input_start;
	size_t i, rune_i;
	const struct fcft_glyph *rune_p;
	const uint32_t *text;
	struct fcft_text_run *run;
	uint32_t codepoint, state = UTF8_ACCEPT;

	// Prompt background
	fill_box_solid(pix_buf, &c->prompt_bg, 0, d->prompt_width, y, y + d->line_height);

	// Prompt text
	if (us->prompt_str) {
		run = us->prompt_run;

		for (x = d->padding, rune_i = 0, rune_p = *run->glyphs; rune_i < run->count;
		     x += rune_p->advance.x, rune_p = run->glyphs[++rune_i]) {

			draw_glyph(pix_buf, rune_p, c->prompt_fg, x, y + d->text_level);
		}

		x += d->padding;
	}
	else x = 0;

	// Input background
	fill_box_solid(pix_buf, &c->bg, x, d->width, y, y + d->line_height);

	x += d->padding;

	input_start = x;
	cursor_pos = input_start;
	i = 1;

	// Input text
	for (char *str_p = us->text; *str_p; str_p++, i++) {
		if (utf8_decode(&state, &codepoint, *str_p)) continue;

		rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
		draw_glyph(pix_buf, rune_p, c->fg, x, y + d->text_level);

		x += rune_p->advance.x;
		if (i == us->cursor) cursor_pos = x;
	}

#if UNDERSCORE_CURSOR
	fill_box_solid(pix_buf, &c->cursor,
		cursor_pos, cursor_pos + 9, y + d->text_level + 1, y + d->text_level + 3);
#else
	fill_box_solid(pix_buf, &c->cursor,
		cursor_pos, cursor_pos + 2, y + 4, y + d->line_height - 4);
#endif

	us->draw_items(pix_buf, us, d, c, font, bottom);

	wl_surface_attach(us->surface, us->current->buffer, 0, 0);
	wl_surface_damage_buffer(us->surface, 0, 0, us->dim.width, us->dim.height);
	wl_surface_commit(us->surface);
}

inline void type_rune(struct uprompt_state *us, xkb_keysym_t sym, char *buf) {
	if (xkb_keysym_to_utf8(sym, buf, 8)) {
		insert(us, buf, strnlen(buf, 8));
		draw_frame(us);
	}
}

size_t next_word(struct uprompt_state *us, int8_t incr) {
	size_t len = strlen(us->text);
	int n = us->cursor + incr;

	for (; us->text[n] == ' '; n += incr) {
		for (; n < len && (us->text[n] & 0xC0) == 0x80; n += incr);
	}
	for (; us->text[n] != ' '; n += incr) {
		for (; n < len && (us->text[n] & 0xC0) == 0x80; n += incr);
	}

	return (0 < incr) ? (len < n ? len : n) : (n < 1 ? 0 : n + 1);
}

size_t next_rune(struct uprompt_state *us, int8_t incr) {
	size_t len = strlen(us->text), n = us->cursor + incr;

	for (; n < len && (us->text[n] & 0xC0) == 0x80; n += incr);
	return n;
}

void keypress(struct uprompt_state *us, enum wl_keyboard_key_state key_state,
xkb_keysym_t sym) {

	if (key_state != WL_KEYBOARD_KEY_STATE_PRESSED) return;

	bool ctrl = xkb_state_mod_name_is_active(us->xkb_state,
		XKB_MOD_NAME_CTRL, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);
	bool shift = xkb_state_mod_name_is_active(us->xkb_state,
		XKB_MOD_NAME_SHIFT, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);
	bool alt = xkb_state_mod_name_is_active(us->xkb_state,
		XKB_MOD_NAME_ALT, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);

	char buf[8];
	size_t len = strlen(us->text);

	switch (sym) {
	case XKB_KEY_KP_Enter:
	case XKB_KEY_Return:
		if (shift) {
			if (us->mode & ModeRestrictRet) break;
			puts(us->text);
			fflush(stdout);
			us->run = RunSuccess;
		}
		else if (ctrl) {
			if (us->mode & ModeRestrictRet) break;
			char *text = us->selection ? us->selection->text : us->text;
			puts(text);
			fflush(stdout);
		}
		else {
			if (us->mode & ModeRestrictRet && !us->selection) break;
			char *text = us->selection ? us->selection->text : us->text;
			puts(text);
			fflush(stdout);
			us->run = RunSuccess;
		}
		break;

	case XKB_KEY_Left:
		if (us->mode & ModeVert || !us->selection) {
			if (us->cursor) {
				us->cursor = ctrl ? next_word(us, -1) : next_rune(us, -1);
			}
		}
		else if (us->selection->left) {
			if (us->selection == us->leftmost) {
				us->rightmost = us->selection->left;
				us->leftmost = NULL;
			}
			us->selection = us->selection->left;
			if (us->matches) us->scroll_matches(us);
		}
		draw_frame(us);
		break;

	case XKB_KEY_Right:
		if (us->mode & ModeVert || !us->selection) {
			if (us->cursor < len) {
				us->cursor = ctrl ? next_word(us, +1) : next_rune(us, +1);
			}
		}
		else if (us->selection->right) {
			if (us->selection == us->rightmost) {
				us->leftmost = us->selection->right;
				us->rightmost = NULL;
			}
			us->selection = us->selection->right;
			if (us->matches) us->scroll_matches(us);
		}
		draw_frame(us);
		break;

	case XKB_KEY_Up:
		if (us->mode & ModeGrid) {
			uint8_t columns = us->dim.width /
				(us->dim.max_item_width + 2 * us->dim.padding);
			for (size_t col = 0; us->selection->left && col < columns; col++) {
				if (us->selection == us->leftmost) {
					us->rightmost = us->selection->left;
					us->leftmost = NULL;
				}
				us->selection = us->selection->left;
			}
			if (us->matches) us->scroll_matches(us);
			draw_frame(us);
		}
		else if (us->mode & ModeVert) {
			if (us->cursor && (!us->selection || !us->selection->left)) {
				us->cursor = next_rune(us, -1);
				draw_frame(us);
			}
			if (us->selection && us->selection->left) {
				if (us->selection == us->leftmost) {
					us->rightmost = us->selection->left;
					us->leftmost = NULL;
				}
				us->selection = us->selection->left;
				if (us->matches) us->scroll_matches(us);
				draw_frame(us);
			}
		}
		break;

	case XKB_KEY_Down:
		if (us->mode & ModeGrid) {
			uint8_t columns = us->dim.width /
				(us->dim.max_item_width + 2 * us->dim.padding);
			for (size_t col = 0; us->selection->right && col < columns; col++) {
				if (us->selection == us->rightmost) {
					us->leftmost = us->selection->right;
					us->rightmost = NULL;
				}
				us->selection = us->selection->right;
			}
			if (us->matches) us->scroll_matches(us);
			draw_frame(us);
		}
		else if (us->mode & ModeVert) {
			if (us->cursor < len) {
				us->cursor = next_rune(us, +1);
				draw_frame(us);
			}
			else if (us->cursor == len) {
				if (us->selection && us->selection->right) {
					if (us->selection == us->rightmost) {
						us->leftmost = us->selection->right;
						us->rightmost = NULL;
					}

					us->selection = us->selection->right;
					if (us->matches) us->scroll_matches(us);
					draw_frame(us);
				}
			}
		}
		break;

	case XKB_KEY_BackSpace:
		if (!us->cursor) break;
		insert(us, NULL,
			((alt || ctrl) ? next_word(us, -1) : next_rune(us, -1)) - us->cursor);
		draw_frame(us);
		break;

	case XKB_KEY_Delete:
		if (us->cursor == len) break;
		size_t cursor_pos = us->cursor;
		us->cursor = (alt || ctrl) ? next_word(us, +1) : next_rune(us, +1);
		insert(us, NULL, cursor_pos - us->cursor);
		draw_frame(us);
		break;

	case XKB_KEY_Tab:
		if (!us->selection) break;
		strncpy(us->text, us->selection->text, sizeof us->text);
		us->cursor = strlen(us->text);
		match(us);
		draw_frame(us);
		break;

	case XKB_KEY_J:
		if (ctrl) {
			if (us->mode & ModeRestrictRet) break;
			puts(us->text);
			fflush(stdout);
			us->run = RunSuccess;
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_j:
		if (ctrl) {
			if (us->mode & ModeRestrictRet && !us->selection) break;
			char *text = us->selection ? us->selection->text : us->text;
			puts(text);
			fflush(stdout);
			us->run = RunSuccess;
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_b:
		if (!ctrl && !alt) type_rune(us, sym, buf);
		else {
			if (!us->cursor) break;
			us->cursor = ctrl ? next_rune(us, -1) : next_word(us, -1);
			draw_frame(us);
		}
		break;

	case XKB_KEY_f:
		if (!ctrl && !alt) type_rune(us, sym, buf);
		else {
			if (len <= us->cursor) break;
			us->cursor = ctrl ? next_rune(us, +1) : next_word(us, +1);
			draw_frame(us);
		}
		break;

	case XKB_KEY_a:
		if (ctrl) {
			us->cursor = 0;
			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_e:
		if (ctrl) {
			us->cursor = len;
			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_l:
		if (ctrl) {
			if (us->cursor < len) us->cursor = len;
			insert(us, NULL, -len);
			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_u:
		if (ctrl) {
			if (us->cursor < 1) break;
			insert(us, NULL, -us->cursor);
			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_k:
		if (ctrl) {
			if (us->cursor == len) break;
			size_t cursor_pos = us->cursor;
			us->cursor = len;
			insert(us, NULL, cursor_pos - len);
			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_w:
		if (ctrl) {
			if (!us->cursor) break;
			insert(us, NULL, next_word(us, -1) - us->cursor);
			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_d:
		if (ctrl) {
			if (us->cursor == len) break;
			us->cursor = next_rune(us, +1);
			insert(us, NULL, next_rune(us, -1) - us->cursor);
			draw_frame(us);
		}
		else if (alt) {
			if (us->cursor == len) break;
			size_t cursor_pos = us->cursor;
			us->cursor = next_word(us, +1);
			insert(us, NULL, cursor_pos - us->cursor);
			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_t:
		if (ctrl) {
			char fst_rune[8], snd_rune[8];
			size_t fst_rune_len, snd_rune_len;

			if (us->cursor == len) {
				if (!us->cursor) break;
				us->cursor = next_rune(us, -1);
				snd_rune_len = len - us->cursor;
				if (!us->cursor) {
					us->cursor = len;
					break;
				}
				us->cursor = next_rune(us, -1);
				fst_rune_len = len - snd_rune_len - us->cursor;
			}
			else if (next_rune(us, +1) == len) {
				if (!us->cursor) break;
				snd_rune_len = len - us->cursor;
				us->cursor = next_rune(us, -1);
				fst_rune_len = len - snd_rune_len - us->cursor;
			}
			else {
				fst_rune_len = -us->cursor;
				fst_rune_len += (us->cursor = next_rune(us, +1));
				snd_rune_len = -us->cursor + next_rune(us, +1);
				us->cursor -= fst_rune_len;
			}

			strncpy(fst_rune, &us->text[us->cursor], fst_rune_len);
			strncpy(snd_rune, &us->text[us->cursor + fst_rune_len], snd_rune_len);
			fst_rune[fst_rune_len] = snd_rune[snd_rune_len] = '\0';

			size_t i;
			for (i = 0; i < snd_rune_len; i++) us->text[us->cursor + i] = snd_rune[i];
			us->cursor += snd_rune_len;
			for (i = 0; i < fst_rune_len; i++) us->text[us->cursor + i] = fst_rune[i];
			us->cursor -= snd_rune_len;

			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_P:
		if (ctrl) {
			char cbd_content[MaxClipboardLen];
			int16_t i, cmd_len;
			for (i = 0; i < MaxClipboardLen; i++) cbd_content[i] = '\0';

			FILE *cbd_f = popen(ClipboardCommand, "r");
			if (cbd_f == NULL) {
				insert(us, "'Nothing'", 6);
				draw_frame(us);
				break;
			}

			fread(cbd_content, MaxClipboardLen, 1, cbd_f);
			pclose(cbd_f);

			cmd_len = strnlen(cbd_content, MaxClipboardLen);
			for (i = cmd_len - 1; 0 <= i; i--) {
				if (cbd_content[i] == '\n' || cbd_content[i] == ' ' ||
				    cbd_content[i] == '\t') cbd_content[i] = '\0';
				else {
					cmd_len = i + 1;
					break;
				}
			}

			for (i--; 0 <= i; i--) if (cbd_content[i] == '\n') cbd_content[i] = ' ';
			insert(us, cbd_content, cmd_len);
			draw_frame(us);
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_bracketleft:
	case XKB_KEY_c:
		if (ctrl) {
			us->run = RunFailure;
		}
		else type_rune(us, sym, buf);
		break;

	case XKB_KEY_Escape:
		us->run = RunFailure;
		break;

	default:
		type_rune(us, sym, buf);
	}
}

void uprompt_create_surface(struct uprompt_state *us) {
	us->surface = wl_compositor_create_surface(us->compositor);
	wl_surface_add_listener(us->surface, &surface_listener, us);
	us->layer_surface = zwlr_layer_shell_v1_get_layer_surface(
		us->layer_shell, us->surface, NULL, ZWLR_LAYER_SHELL_V1_LAYER_TOP, PROGNAME);
	assert(us->layer_surface != NULL);

	uint32_t anchor =
		ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT;

	anchor |= us->mode & ModeBottom ?
		ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM : ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP;

	zwlr_layer_surface_v1_set_anchor(us->layer_surface, anchor);
	zwlr_layer_surface_v1_set_size(us->layer_surface, 0, us->dim.height);
	zwlr_layer_surface_v1_set_exclusive_zone(us->layer_surface, -1);
	zwlr_layer_surface_v1_set_keyboard_interactivity(us->layer_surface, true);
	zwlr_layer_surface_v1_add_listener(us->layer_surface, &layer_surface_listener, us);

	wl_surface_commit(us->surface);
	wl_display_roundtrip(us->display);
}

inline void canvas_fini(struct pix_canvas *canvas) {
	pixman_image_unref(canvas->fg);
	pixman_image_unref(canvas->sel_fg);
	pixman_image_unref(canvas->prompt_fg);
}

struct pix_canvas canvas_init(struct hex_colors *hex) {
	const pixman_color_t fill_fg        = hex2pixman(hex->fg);
	const pixman_color_t fill_sel_fg    = hex2pixman(hex->sel_fg);
	const pixman_color_t fill_prompt_fg = hex2pixman(hex->prompt_fg);

	return (struct pix_canvas) {
		.bg         = hex2pixman(hex->bg),
		.sel_bg     = hex2pixman(hex->sel_bg),
		.prompt_bg  = hex2pixman(hex->prompt_bg),
		.cursor     = hex2pixman(hex->cursor),
		.border     = hex2pixman(hex->border),
		.fg        = pixman_image_create_solid_fill(&fill_fg),
		.sel_fg    = pixman_image_create_solid_fill(&fill_sel_fg),
		.prompt_fg = pixman_image_create_solid_fill(&fill_prompt_fg),
	};
}

void uprompt_init(struct uprompt_state *us, struct dimensions *dim) {
	if (us->mode <= ModeBottom) us->mode |= ModeHoriz;

	dim->height = (1 + dim->lines) * dim->line_height + dim->border_width;
	dim->padding = dim->line_height / 2;
	dim->arrow_width = dim->line_height * 2;
	us->canvas = canvas_init(&us->hex);

	us->display = wl_display_connect(NULL);
	if (!us->display) {
		canvas_fini(&us->canvas);
		eprintf("Could not connect to wayland display\n");
	}

	us->xkb_context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	if (!us->xkb_context) {
		canvas_fini(&us->canvas);
		wl_display_disconnect(us->display);
		eprintf("Could not create xkb context\n");
	}

	us->repeat_timer = timerfd_create(CLOCK_MONOTONIC, 0);
	assert(0 <= us->repeat_timer);

	struct wl_registry *registry = wl_display_get_registry(us->display);
	wl_registry_add_listener(registry, &registry_listener, us);
	wl_display_roundtrip(us->display);

	assert(us->compositor != NULL);
	assert(us->layer_shell != NULL);
	assert(us->shm != NULL);
	assert(us->output_manager != NULL);

	// Second roundtrip for xdg-output
	wl_display_roundtrip(us->display);

	if (us->output_name && !us->output) {
		canvas_fini(&us->canvas);
		compositor_fini(us);
		eprintf("Output %s not found\n", us->output_name);
	}
}

void font_init(struct uprompt_state *us) {
	fcft_init(FCFT_LOG_COLORIZE_AUTO, 8, FCFT_LOG_CLASS_ERROR);
	fcft_set_scaling_filter(FCFT_SCALING_FILTER_LANCZOS3);

	us->font = fcft_from_name(FONT_NUMBER, (const char **)(*us->fn_names), NULL);
	if (!us->font) eprintf("Could not load font[s]!\n");

	// Convert prompt_str to utf32
	if (!us->prompt_str) return;

	size_t str_len = strlen(us->prompt_str);
	uint32_t runes[str_len - 1];
	size_t rune_num = 0;
	uint32_t codepoint, last_cp = 0, state = UTF8_ACCEPT;
	uint16_t advance = 0;

	for (char *str_p = us->prompt_str; *str_p; str_p++) {
		if (utf8_decode(&state, &codepoint, *str_p)) continue;

		runes[rune_num] = codepoint;
		rune_num++;
	}

	us->prompt_run = fcft_rasterize_text_run_utf32(us->font,
		rune_num, runes, FCFT_SUBPIXEL_NONE);

	for (size_t i = 0; i < rune_num; i++) {
		advance += us->prompt_run->glyphs[i]->advance.x;
	}

	us->dim.prompt_width = advance + 2 * us->dim.padding;
}

inline void surface_fini(struct uprompt_state *us) {
	zwlr_layer_surface_v1_destroy(us->layer_surface);
	wl_surface_destroy(us->surface);
	zwlr_layer_shell_v1_destroy(us->layer_shell);
}

inline void font_fini(struct uprompt_state *us) {
	if (us->prompt_str) fcft_text_run_destroy(us->prompt_run);
	fcft_destroy(us->font);
	fcft_fini();
}

inline void compositor_fini(struct uprompt_state *us) {
	wl_compositor_destroy(us->compositor);
	wl_shm_destroy(us->shm);
	wl_keyboard_destroy(us->keyboard);
	zxdg_output_manager_v1_destroy(us->output_manager);
	wl_display_disconnect(us->display);
}

void uprompt_fini(struct uprompt_state *us) {
	canvas_fini(&us->canvas);
	font_fini(us);
	surface_fini(us);
	compositor_fini(us);
	if (!(us->mode & ModeEnter)) free_items(us);
}

void insert(struct uprompt_state *us, const char *s, ssize_t n) {
	if (sizeof us->text - 1 < strlen(us->text) + n) return;

	memmove(us->text + us->cursor + n, us->text + us->cursor,
		sizeof us->text - us->cursor - (0 < n ? n : 0));

	if (n > 0) memcpy(us->text + us->cursor, s, n);

	us->cursor += n;
	match(us);
}

void scroll_grid_matches(struct uprompt_state *us) {
	uint16_t offs = us->dim.line_height;
	struct uprompt_item *item;
	uint8_t columns = us->dim.width / (us->dim.max_item_width + 2 * us->dim.padding);
	size_t col;

	if (us->leftmost == NULL) {
		us->leftmost = us->matches;

		for (item = us->rightmost; item; offs += us->dim.line_height) {
			if (offs >= us->dim.height - us->dim.border_width) {
				us->leftmost = item->right;
				break;
			}

			for (col = 0; item && col < columns; col++, item = item->left);
		}
	}
	else if (us->rightmost == NULL) {
		us->rightmost = us->matches;

		for (item = us->leftmost; item; offs += us->dim.line_height) {
			if (offs >= us->dim.height - us->dim.border_width) break;

			for (col = 0; item && col < columns; col++,
			     item = item->right) us->rightmost = item;
		}
	}
}

void scroll_vertical_matches(struct uprompt_state *us) {
	uint16_t offs = us->dim.line_height;
	struct uprompt_item *item;

	if (us->leftmost == NULL) {
		us->leftmost = us->matches;

		for (item = us->rightmost; item;
		     item = item->left, offs += us->dim.line_height) {

			if (offs >= us->dim.height - us->dim.border_width) {
				us->leftmost = item->right;
				break;
			}
		}
	}
	else if (us->rightmost == NULL) {
		us->rightmost = us->matches;

		for (item = us->leftmost; item;
		     item = item->right, offs += us->dim.line_height) {

			if (offs >= us->dim.height - us->dim.border_width) break;
			us->rightmost = item;
		}
	}
}

void scroll_horizontal_matches(struct uprompt_state *us) {
	// Calculate available space
	int padding = us->dim.padding;
	int width = us->dim.width - us->dim.input_width
	          - us->dim.prompt_width - 2 * us->dim.arrow_width;
	uint16_t offs = 0;
	struct uprompt_item *item;

	if (us->leftmost == NULL) {
		us->leftmost = us->matches;

		for (item = us->rightmost; item; item = item->left) {
			offs += item->width + 2 * padding;

			if (offs >= width) {
				us->leftmost = item->right;
				break;
			}
		}
	}
	else if (us->rightmost == NULL) {
		us->rightmost = us->matches;

		for (item = us->leftmost; item; item = item->right) {
			offs += item->width + 2 * padding;

			if (offs >= width) break;
			us->rightmost = item;
		}
	}
}

void append_item(struct uprompt_item *item,
struct uprompt_item **list, struct uprompt_item **last) {

	if (!*last) *list = item;
	else (*last)->right = item;

	item->left = *last;
	item->right = NULL;
	*last = item;
}

char *fstrstr(struct uprompt_state *us, const char *s, const char *sub) {
	for (size_t len = strlen(sub); *s; s++) {
		if (!us->fstrncmp(s, sub, len)) return (char *)s;
	}

	return NULL;
}

void match(struct uprompt_state *us) {
	struct uprompt_item *item, *itemend, *lexact, *lprefix,
	                 *lsubstr, *exactend, *prefixend, *substrend;

    us->matches = NULL;
    us->leftmost = NULL;
    size_t len = strlen(us->text);
    us->matches = lexact = lprefix = lsubstr = itemend
                = exactend = prefixend = substrend = NULL;

    for (item = us->items; item; item = item->next) {
    	if (!us->fstrncmp(us->text, item->text, len + 1)) {
    		append_item(item, &lexact, &exactend);
    	}
    	else if (!us->fstrncmp(us->text, item->text, len)) {
    		append_item(item, &lprefix, &prefixend);
    	}
    	else if (fstrstr(us, item->text, us->text)) {
    		append_item(item, &lsubstr, &substrend);
    	}
    }

    if (lexact) {
    	us->matches = lexact;
    	itemend = exactend;
    }

    if (lprefix) {
    	if (itemend) {
    		itemend->right = lprefix;
    		lprefix->left = itemend;
    	}
    	else us->matches = lprefix;
    	itemend = prefixend;
    }

    if (lsubstr) {
    	if (itemend) {
    		itemend->right = lsubstr;
    		lsubstr->left = itemend;
    		itemend = substrend;
    	}
    	else us->matches = lsubstr;
    }

    us->selection = us->matches;

    if (us->mode & ModeInstant && us->matches && us->matches == itemend && !lsubstr) {
    	puts(us->matches->text);
    	us->run = false;
    }

	us->leftmost = us->matches;
	us->rightmost = NULL;

	if (us->matches) us->scroll_matches(us);
}

void free_items(struct uprompt_state *us) {
	struct uprompt_item *item;
	for (item = us->items; item; item = item->right) {
		free(item->text);
		free(item);
	}
}

void read_stdin(struct uprompt_state *us) {
	char buf[sizeof us->text], *p;
	struct uprompt_item *item, **end;

	for (end = &us->items;
		fgets(buf, sizeof buf, stdin); *end = item, end = &item->next) {

		if((p = strchr(buf, '\n'))) *p = '\0';
		item = malloc(sizeof *item);
		if (!item) return;

		item->text = strdup(buf);
		item->next = item->left = item->right = NULL;
		item->len = strlen(item->text);
		item->width = get_text_advance(us->font, item->text);

		if (us->dim.max_item_width < item->width)
			us->dim.max_item_width = item->width;
	}
	if (us->dim.input_width < us->dim.max_item_width)
		us->dim.input_width = us->dim.max_item_width;
}

int main(int argc, char **argv) {
	struct uprompt_state us = {
		.mode = ModeNone,
		.fstrncmp = strncasecmp,
		.fn_names = FnNames,

		.hex = {
			.bg        = ColBg,       .fg        = ColFg,
			.sel_bg    = ColSelBg,    .sel_fg    = ColSelFg,
			.prompt_bg = ColPromptBg, .prompt_fg = ColPromptFg,
			.cursor    = ColCursor,   .border    = ColBorder,
		},
		.dim = {
			.line_height    = DimLineHeight,
			.text_level     = DimTextLevel,
			.input_width    = DimInputWidth,
			.border_width   = BorderWidth,
			.lines          = 0,
			.max_item_width = 0,
		},

		.scroll_matches = scroll_horizontal_matches,
		.draw_items = draw_horizontal_items,
		.run = RunRunning,
	};

	const char *usage =
		"Usage: uprompt [-birsev] [-fn font] [-l lines] [-g lines]\n"
		"       [-o output] [-p prompt] [-h height[:text_level]]\n"
		"       [-nb,nf color] [-pb,pf color] [-sb,sf color] [-cc,bc color]\n";

	for (size_t i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-v")) { puts(VERSION); return 0; }
		else if (!strcmp(argv[i], "-b")) us.mode |= ModeBottom;
		else if (!strcmp(argv[i], "-i")) us.mode |= ModeInstant;
		else if (!strcmp(argv[i], "-r")) us.mode |= ModeRestrictRet;
		else if (!strcmp(argv[i], "-s")) us.fstrncmp = strncmp;
		else if (!strcmp(argv[i], "-e")) us.mode |= ModeEnter;
		else if (i == argc - 1) goto print_usage;
		else if (!strcmp(argv[i], "-l")) { us.mode |= ModeVert;
		                                   us.dim.lines = atoi(argv[++i]);
		                                   us.scroll_matches = scroll_vertical_matches;
		                                   us.draw_items = draw_vertical_items;
		}
		else if (!strcmp(argv[i], "-g")) { us.mode |= ModeGrid;
		                                   us.dim.lines = atoi(argv[++i]);
		                                   us.scroll_matches = scroll_grid_matches;
		                                   us.draw_items = draw_grid_items;
		}
		else if (!strcmp(argv[i], "-bw")) us.dim.border_width = atoi(argv[++i]);
		else if (!strcmp(argv[i], "-h")) set_line_height(&us.dim, argv[++i]);
		else if (!strcmp(argv[i], "-o")) us.output_name = argv[++i];
		else if (!strcmp(argv[i], "-p")) us.prompt_str = argv[++i];
		else if (!strcmp(argv[i], "-fn")) *(us.fn_names)[0] = argv[++i];
		else if (!strcmp(argv[i], "-nb")) parse_hex_color(argv[++i], &us.hex.bg);
		else if (!strcmp(argv[i], "-nf")) parse_hex_color(argv[++i], &us.hex.fg);
		else if (!strcmp(argv[i], "-pb")) parse_hex_color(argv[++i], &us.hex.prompt_bg);
		else if (!strcmp(argv[i], "-pf")) parse_hex_color(argv[++i], &us.hex.prompt_fg);
		else if (!strcmp(argv[i], "-sb")) parse_hex_color(argv[++i], &us.hex.sel_bg);
		else if (!strcmp(argv[i], "-sf")) parse_hex_color(argv[++i], &us.hex.sel_fg);
		else if (!strcmp(argv[i], "-cc")) parse_hex_color(argv[++i], &us.hex.cursor);
		else if (!strcmp(argv[i], "-bc")) parse_hex_color(argv[++i], &us.hex.border);
		else goto print_usage;
	}
	uprompt_init(&us, &us.dim);
	uprompt_create_surface(&us);
	font_init(&us);

	if (!(us.mode & ModeEnter)) read_stdin(&us);
	match(&us);
	draw_frame(&us);

	struct pollfd fds[] = {
		{ wl_display_get_fd(us.display), POLLIN },
		{ us.repeat_timer, POLLIN },
	};

	const int nfds = sizeof(fds) / sizeof(*fds);

	while (us.run == RunRunning) {
		errno = 0;
		do {
			if (wl_display_flush(us.display) == -1 && errno != EAGAIN) {
				uprompt_fini(&us);
				eprintf("wl_display_flush: %s\n", strerror(errno));
			}
		} while (errno == EAGAIN);

		if (poll(fds, nfds, -1) < 0) {
			uprompt_fini(&us);
			eprintf("poll: %s\n", strerror(errno));
		}

		if (fds[0].revents & POLLIN) {
			if (wl_display_dispatch(us.display) < 0) us.run = false;
		}

		if (fds[1].revents & POLLIN) keyboard_repeat(&us);
	}

	uprompt_fini(&us);

	return us.run == RunFailure ? ExitFailure : ExitSuccess;

print_usage:
	fprintf(stderr, "%s", usage);
	return 1;
}
